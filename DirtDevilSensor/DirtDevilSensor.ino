/**
Start domoticz to test:
  docker run -d -p 8080:8080 --name=domoticz -v /tmp/domoticz:/config -v /etc/localtime:/etc/localtime:ro sdesbure/domoticz
*/

#define MY_DEBUG
#define MY_RADIO_NRF24

#include <SPI.h>
#include <MySensors.h> 
#include <Relay.h>  
#include <LDR.h>  

// MySensors children
#define CHILD_ID_GO_WHEN_READY      0
#define CHILD_ID_ROBOT_HOME         1
#define CHILD_ID_VOLT_SENSOR        2
#define CHILD_ID_BATTERY            3

// Pin layout
#define PIN_RELAIS                  3
#define PIN_LDR                     A0
#define PIN_VOLTAGE                 A3
#define PIN_REFLECTION              A4

// variaous delay lengths
#define SEND_UPDATE_MILLIS          120000
#define GENERIC_DELAY_MILLIS        1000
#define LRD_READ_MILLIS             600
#define VOLTAGE_READ_MILLIS         15000
#define RELAIS_DURATION             600
#define HOME_TRESHOLD               995
#define NOT_HOME_TRESHOLD           1000
#define HOME_MILLIS                 5000
#define NOT_HOME_MILLIS             5000
#define RESEND_HOME_INTERVAL        60000

// Analog accuracy
#define LDR_ACCURACY                500
#define VOLTAGE_ACCURACY            2
#define VOLTAGE_READ_COUNT          4
#define REFLECTION_ACCURACY         2
#define REFLECTION_READ_COUNT       4
#define REFLECTION_UPDATE_MILLIS    1000

// Analog threshold values
#define GO_WHEN_READY_OFF           0 
#define GO_WHEN_READY_ON            200

// Define values for calculating voltage in battery
#define DIRT_DEVIL_BATTERY_VOLTAGE  19.0f
#define STEP_DOWN_FACTOR            10
#define VOLTAGE_OFFSET              0.42f

Relais* relais;
LDR* ldr; 
AnalogInput* voltage;
AnalogInput* reflection;

MyMessage msgGo(CHILD_ID_GO_WHEN_READY, V_STATUS);
MyMessage msgRobotHome(CHILD_ID_ROBOT_HOME, V_STATUS);
MyMessage msgVoltage(CHILD_ID_VOLT_SENSOR, V_VOLTAGE);
MyMessage msgBattery(CHILD_ID_BATTERY, V_VOLTAGE);

void presentation()  
{ 
  sendSketchInfo("DirtDevil", "1.0");
  present(CHILD_ID_GO_WHEN_READY, S_BINARY);
  present(CHILD_ID_ROBOT_HOME, S_BINARY);
  present(CHILD_ID_VOLT_SENSOR, S_MULTIMETER);
  present(CHILD_ID_BATTERY, S_MULTIMETER);
}

void setup()
{
  Serial.println("Starting DirtDevil");
  #ifdef MY_DEBUG
    Serial.println("WARNING: Running with debug, incoming messages may not be received or to be delayed");
  #endif
  
  relais = new Relais(PIN_RELAIS, true);
  relais->turnOff();
  
  ldr = new LDR(PIN_LDR, LDR_ACCURACY, SEND_UPDATE_MILLIS, LRD_READ_MILLIS);
  
  voltage = new AnalogInput(PIN_VOLTAGE, VOLTAGE_ACCURACY, SEND_UPDATE_MILLIS, VOLTAGE_READ_MILLIS, false);
  voltage->setVoltageMultiplier(STEP_DOWN_FACTOR);
  voltage->setVoltageOffset(VOLTAGE_OFFSET);
  voltage->setReadCount(VOLTAGE_READ_COUNT);

  reflection = new AnalogInput(PIN_REFLECTION, REFLECTION_ACCURACY, REFLECTION_UPDATE_MILLIS, REFLECTION_READ_COUNT, true);
  
  send(msgGo.set(LOW));
  delay(1000);
}

void receive(const MyMessage &message) {
  #ifdef MY_DEBUG
    Serial.print("Received message for sensor ");
    Serial.print(message.sensor);
    Serial.print(": ");
    Serial.println(message.getString());
  #endif

  if(message.sensor == CHILD_ID_GO_WHEN_READY) {
    if(message.getInt() == HIGH) {
      relais->turnOn();
    }
  }
}

void loop()      
{  
  if(relais->isOn() && millis() - relais->getTurnedOnMillis() > RELAIS_DURATION) {
    relais->turnOff();
    delay(PIN_LDR);
  }

  robotHome();
  on24HourSchedule();
  batteryPower();
  
  #ifdef MY_DEBUG
    ldr->debugPrint();
    relais->debugPrint();
    voltage->debugPrint();
    reflection->debugPrint();
  #endif
  
  delay(GENERIC_DELAY_MILLIS);
}

void robotHome() {
  static boolean isHome = false;
  static unsigned long lastHome = 0;
  static unsigned long lastNotHome = 0;

  if(reflection->update(millis())) {
    if(isHome && reflection->getValue() >= NOT_HOME_TRESHOLD) {
      isHome = false;
      lastNotHome = millis();
    }
    else if(!isHome && reflection->getValue() <= HOME_TRESHOLD) {
      isHome = true;
      lastHome = millis();
    }
    else {
      #ifdef MY_DEBUG
        Serial.print("Could not use value for home detection: ");
        Serial.println(reflection->getValue());
      #endif
    }
  }
  
  if(isHome && millis() > lastHome && millis() - lastHome > HOME_MILLIS) {
    #ifdef MY_DEBUG
      Serial.println("Robot home");
    #endif
    lastHome = millis() + RESEND_HOME_INTERVAL;
    send(msgRobotHome.set(HIGH));
  }
  else if(!isHome && millis() > lastNotHome && millis() - lastNotHome > NOT_HOME_MILLIS) {
    #ifdef MY_DEBUG
      Serial.println("Robot not home");
    #endif
    lastNotHome = millis() + RESEND_HOME_INTERVAL;
    send(msgRobotHome.set(LOW));
  }
}

void on24HourSchedule() {
  if(ldr->update(millis())) {
    if(buttonsAreOff()) {
      send(msgGo.set(LOW));
    }
    else if(goWhenReady()) {
      send(msgGo.set(HIGH));
    }
  }
}

void batteryPower() {
  static float lastKnownBatteryVoltage = 0.0f;
  static float batteryVoltage = 0.0f;
  
  if(voltage->update(millis())) {
    send(msgVoltage.set(voltage->getVoltage(), 3));
    
    batteryVoltage = DIRT_DEVIL_BATTERY_VOLTAGE - 
      (2 * (DIRT_DEVIL_BATTERY_VOLTAGE - (voltage->getVoltage()))) -
      (voltage->getVoltageOffset() / 2);

    if(reflection->getValue() < HOME_TRESHOLD) {
      if(lastKnownBatteryVoltage > 0.0f) {
        send(msgBattery.set(lastKnownBatteryVoltage, 1));  
      }
    }
    else {
      // Only send the voltage when the dirt devil is home
      // We dont know the battery power if it's not home
      lastKnownBatteryVoltage = batteryVoltage;
      send(msgBattery.set(batteryVoltage, 1));  
    }
  }

  #ifdef MY_DEBUG
    Serial.print("Battery power: ");
    Serial.print(batteryVoltage, 1);
    Serial.println("V");
  #endif
}

bool buttonsAreOff() {
  return ldr->getValue() >= GO_WHEN_READY_OFF && ldr->getValue() <= GO_WHEN_READY_ON;
}

bool goWhenReady() {
  return ldr->getValue() > GO_WHEN_READY_ON;
}

