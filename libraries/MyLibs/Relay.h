#ifndef SENSOR_BASE_LOADED
    #include <SensorBase.h>
#endif

class Relais {
  private:
    int pin;
    bool normallyOpen;
    bool on = false;
    unsigned long turnedOnAt = 0;
    unsigned long turnedOffAt = 0;

    void write() {
      digitalWrite(pin, on);
    }

  public:
    /**
     * Create relais on given pin.
     * alwaysOn defines how the relay is connected.
     * See: http://www.circuitbasics.com/setting-up-a-5v-relay-on-the-arduino/
     *
     * alwaysOn true if NO and C are used.
     * alwaysOn false if NC and C are used.
     */
    Relais(int pin, bool normallyOpen) {
      pinMode(pin, OUTPUT);
      this->pin = pin;
      this->normallyOpen = normallyOpen;
      write();

      #ifdef MY_DEBUG
        Serial.print("Relais on pin ");
        Serial.println(pin);
      #endif
    }

    bool getOn() {
      return isOn();
    }

    bool isOn() {
      if(normallyOpen)
        return on;
      else
        return !on;
    }

    void turnOn() {
      setOn(true);
    }

    void turnOff() {
      setOn(false);
    }

    unsigned long getTurnedOnMillis() {
      return this->turnedOnAt;
    }

    unsigned long getTurnedOffMillis() {
      return this->turnedOffAt;
    }

    void setOn(bool v) {
      if(normallyOpen) {
          if(v && !on) {
            this->turnedOnAt = millis();
            Serial.print("Turning on relais on pin ");
            Serial.println(pin);
          } else if(!v && on) {
            this->turnedOffAt = millis();
            Serial.print("Turning off relais on pin ");
            Serial.println(pin);
          }
        on = v;
      }
      else {
          if(v && on) {
            this->turnedOnAt = millis();
            Serial.print("Turning on relais on pin ");
            Serial.println(pin);
          } else if(!v && !on) {
            this->turnedOffAt = millis();
            Serial.print("Turning off relais on pin ");
            Serial.println(pin);
          }
        on = !v;
      }

      write();
    }

    #ifdef MY_DEBUG
    void debugPrint() {
      Serial.print("Relais on pin ");
      Serial.print(pin);
      Serial.print(": V=");
      Serial.println(isOn());
    }
    #endif
};
