class EEPROM {
    public:

        /**
         *  Saves an unsigned long to 4 addresses.
         */
        static void setUnsignedLong(int address, unsigned long value)
        {
          //Decomposition from a long to 4 bytes by using bitshift.
          //One = Most significant -> Four = Least significant byte
          byte four = (value & 0xFF);
          byte three = ((value >> 8) & 0xFF);
          byte two = ((value >> 16) & 0xFF);
          byte one = ((value >> 24) & 0xFF);

          //Write the 4 bytes into the eeprom memory.
          saveState(address, four);
          saveState(address + 1, three);
          saveState(address + 2, two);
          saveState(address + 3, one);
        }

        /**
         *  Loads an unsigned long from 4 addresses.
         */
        static unsigned long getUnsignedLong(long address)
        {
          //Read the 4 bytes from the eeprom memory.
          long four = loadState(address);
          long three = loadState(address + 1);
          long two = loadState(address + 2);
          long one = loadState(address + 3);

          //Return the recomposed long by using bitshift.
          return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
        }

        /**
         *  Saves a double to 4 addresses, with a 4 digit precision.
         */
        static void setDouble(int address, double value)
        {
          setUnsignedLong(address, value * 1000);
        }

        /**
         *  Loads a double from 4 addresses, with a 4 digit precision.
         */
        static double getDouble(int address)
        {
          return getUnsignedLong(address) / 1000.0;
        }
};