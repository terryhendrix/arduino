#ifndef SENSOR_BASE_LOADED
    #include <SensorBase.h>
#endif

class MotionSensor : public Sensor {
  private:
    IntValue* sensor = new IntValue();
    int pin;
    unsigned long lastDetectionMillis = 0;
    bool armed = true;
    bool hasUpdated = false;
    unsigned long lastUpdateMillis = 0;
    unsigned long forceUpdateEveryMillis;

  public:
    MotionSensor(int pin, unsigned long forceUpdateEveryMillis = 60000) {
      pinMode(pin, INPUT);
      this->pin = pin;
      this->forceUpdateEveryMillis = forceUpdateEveryMillis;

      #ifdef MY_DEBUG
        Serial.print("MotionSensor on pin ");
        Serial.println(pin);
      #endif
    }

    void setArmed(bool armed) {
      #ifdef MY_DEBUG
        if(armed)
          Serial.println("Armed motion sensor");
        else
          Serial.println("Disarmed motion sensor");
      #endif

      this->armed = armed;
    }

    bool isArmed() {
      return armed;
    }

    bool update(unsigned long epoch) {
      sensor->setValue(digitalRead(pin));

      if(sensor->getValue() == HIGH) {
        lastDetectionMillis = epoch;
      }

      if(epoch - lastUpdateMillis > forceUpdateEveryMillis || sensor->hasUpdated() || !hasUpdated) {
        lastUpdateMillis = epoch;
        hasUpdated = true;
        return true;
      }
      else {
        return false;
      }
    }

    unsigned long getLastDetectionMillis() {
      return lastDetectionMillis;
    }

    bool isMovementDetected() {
      return sensor->getValue() == HIGH;
    }

    void debugPrint() {
      Serial.print("MotionSensor on pin ");
      Serial.print(pin);
      Serial.print(": V=");
      Serial.println(sensor->getValue());
    }
};
