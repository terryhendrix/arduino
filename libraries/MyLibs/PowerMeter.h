#include <SensorBase.h>

class PowerMeter  {
    private:
        const unsigned long MILLIS_IN_HOUR = 3600000;
        unsigned long pulsesPerKwh, sampleIntervalMillis;

        Sampler* sampler;
        Counter* high = new Counter();
        Counter* low = new Counter();

        bool isHigh = true;

    public:
        PowerMeter(unsigned long pulsesPerKwh, unsigned long sampleIntervalMillis) {
            this->pulsesPerKwh = pulsesPerKwh;
            this->sampleIntervalMillis = sampleIntervalMillis;
            this->sampler = new Sampler(sampleIntervalMillis);
        }

        double getKwhLow() {
            return factor(low->getTicks());
        }

        void setKwhLow(double kwh) {
            low->setTicks(kwh * pulsesPerKwh);
        }

        double getKwhHigh() {
            return factor(high->getTicks());
        }


        void setKwhHigh(double kwh) {
            high->setTicks(kwh * pulsesPerKwh);
        }

        int getWattSample() {
            Serial.print("Got sample of ");
            Serial.println(this->sampler->getSample());
            return (double) this->sampler->getSample() / (double) pulsesPerKwh * 1000.0 * ((double) MILLIS_IN_HOUR / (double) sampleIntervalMillis);
        }

        unsigned long getTicks() {
            return low->getTicks() + high->getTicks();
        }

        double getKwh() {
            return getKwhHigh() + getKwhLow();
        }

        Sampler* getSampler() {
            return sampler;
        }

        void tick() {
            sampler->sample();

            if(isHigh) {
                high->tick();
            }
            else {
                low->tick();
            }
        }

        void setHigh() {
            isHigh = true;
        }

        void setLow() {
            isHigh = false;
        }

        bool isHighActive() {
            return isHigh;
        }

        double factor(unsigned long ticks) {
            return (double) ticks / (double) pulsesPerKwh;
        }
};
