// Tell subclasses we've already loaded the sensor base, so they are not #included again.

#ifndef SENSOR_BASE_LOADED

class Sensor {
  public:
    bool update(unsigned long epoch);
};


class BoolValue {
  private:
    bool sensorValue, prevValue;
    bool timeout;

  public:
    boolean hasUpdated() {
      return sensorValue != prevValue;
    };

    void setValue(bool value) {
      prevValue = sensorValue;
      sensorValue = value;
    }

    bool getValue() {
      return sensorValue;
    }
};

class IntValue {
  private:
    int sensorValue, prevValue;
    int timeout;

  public:
    boolean hasUpdated() {
      return sensorValue != prevValue;
    };

    void setValue(int value) {
      prevValue = sensorValue;
      sensorValue = value;
    }

    int getValue() {
      return sensorValue;
    }
};

class FloatValue {
  private:
    float sensorValue, prevValue;
    float timeout;

  public:
    boolean hasUpdated() {
      return sensorValue != prevValue;
    };

    void setValue(float value) {
      prevValue = sensorValue;
      sensorValue = value;
    }

    float getValue() {
      return sensorValue;
    }
};

class Format {
  public:
    static String hhmmssTime(unsigned long epoch) {
      int seconds = epoch / 1000L % 60;
      int minutes = epoch / 1000L / 60L % 60L;
      int hours = epoch / 1000L / 60L / 60L;
      String time = "";
      time += zeroPad(hours);
      time += ":";
      time += zeroPad(minutes);
      time += ":";
      time += zeroPad(seconds);
      return time;
    }

    static String zeroPad(int v) {
      String s = "";
      if(v < 10) {
        s += "0";
      }
      s += v;
      return s;
    }
};


class TimeTrigger {
    private:
        unsigned long lastCheckMillis = 0,
                      checkEveryMillis;
    public:
        TimeTrigger(unsigned long checkEveryMillis) {
            this->checkEveryMillis = checkEveryMillis;
        }

        void setTriggered(unsigned long epoch) {
            lastCheckMillis = epoch;
        }

        bool isTriggered(unsigned long epoch) {
            if(epoch - lastCheckMillis > checkEveryMillis) {
                setTriggered(epoch);
                return true;
            }
            else {
                return false;
            }
        }
};

class Counter {
    private:
        unsigned long ticks = 0;
    public:
        void tick() {
            ticks++;
        }

        unsigned long getTicks() {
            return ticks;
        }

        void resetTicks() {
          setTicks(0);
        }

        void setTicks(unsigned long ticks) {
          this->ticks = ticks;
        }
};


class Sampler {
    private:
        Counter* sampler = new Counter();
        TimeTrigger* sampleTimer;
        unsigned long lastSample = 0;

    public:
        Sampler(unsigned long sampleIntervalMillis) {
            sampleTimer = new TimeTrigger(sampleIntervalMillis);
        }

        void sample() {
            sampler->tick();
        }

        unsigned long getSample() {
            return lastSample;
        }

        bool isSampleComplete() {
            if(sampleTimer->isTriggered(millis())) {
                lastSample = sampler->getTicks();
                sampler->resetTicks();
                return true;
            }

            return false;
        }
};

#endif

#define SENSOR_BASE_LOADED
