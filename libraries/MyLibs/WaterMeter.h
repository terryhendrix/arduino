#include <SensorBase.h>

class WaterMeter  {
    private:
        const unsigned long MILLIS_IN_HOUR = 3600000;
        unsigned long pulsesPerCubicMeter, sampleIntervalMillis;

        Sampler* sampler;
        Counter* total = new Counter();

        bool isHigh = true;

    public:
        WaterMeter(unsigned long pulsesPerCubicMeter, unsigned long sampleIntervalMillis) {
            this->pulsesPerCubicMeter = pulsesPerCubicMeter;
            this->sampleIntervalMillis = sampleIntervalMillis;
            this->sampler = new Sampler(sampleIntervalMillis);
        }

        void tick() {
            sampler->sample();
            total->tick();
        }

        void setCubicMeters(double cubics) {
            total->setTicks(cubics * pulsesPerCubicMeter);
        }

        double getCubicMeters() {
            return (double) total->getTicks() / (double) pulsesPerCubicMeter;
        }

        int getLiterSample() {
            Serial.print("Got sample of ");
            Serial.println(this->sampler->getSample());
            return (double) this->sampler->getSample() / (double) pulsesPerCubicMeter * 1000.0 * ((double) MILLIS_IN_HOUR / (double) sampleIntervalMillis);
        }

        unsigned long getTicks() {
            return total->getTicks();
        }

        Sampler* getSampler() {
            return sampler;
        }
};
