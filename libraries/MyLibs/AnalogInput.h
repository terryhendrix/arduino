#include <SensorBase.h>

#ifndef ANALOG_INPUT_LOADED

/**
    Measure analog input with a specific accuracy
*/
class AnalogInput : public Sensor {
    protected:
        int pin;
        int accuracy;
        IntValue* input = new IntValue();
        IntValue* raw = new IntValue();
        TimeTrigger* updateTimer;
        TimeTrigger* readTimer;
        bool firstReadPerformed = false;
        float voltageMultiplier = 1;
        float readCount = 4;
        float voltageOffset = 0;
        int delayMillisAfterRead = 30;

    public:
        AnalogInput(int pin, int accuracy = 1, unsigned long forceUpdateEveryMillis = 60000, unsigned long readIntervalMillis = 1, bool pullup = false) {
            pinMode(pin, INPUT);
            this->pin = pin;
            this->updateTimer = new TimeTrigger(forceUpdateEveryMillis);
            this->readTimer = new TimeTrigger(readIntervalMillis);
            this->accuracy = accuracy;

            if(pullup) {
                pinMode(pin, INPUT_PULLUP);
            }
            else {
                pinMode(pin, INPUT);
            }
        }

        bool update(unsigned long epoch) {
            if(readTimer->isTriggered(epoch) || !firstReadPerformed) {
                firstReadPerformed = true;
                int acc = 0;

                for(int i = 0; i < readCount; i++) {
                    acc += analogRead(this->pin);
                    delay(delayMillisAfterRead);
                }

                raw->setValue(acc / readCount);
                input->setValue(raw->getValue() / accuracy * accuracy);
            }
            else {
                input->setValue(input->getValue());
            }

            if(updateTimer->isTriggered(epoch) || input->hasUpdated()) {
                return true;
            }
            else {
                return false;
            }
        }

        void setMillisDelayAfterRead(int delayMillisAfterRead) {
            this->delayMillisAfterRead = delayMillisAfterRead;
        }

        void setReadCount(int readCount) {
            this->readCount = readCount;
        }

        bool hasUpdated() {
            return input->hasUpdated();
        }

        int getValue() {
            return input->getValue();
        }


        int getRawValue() {
            return raw->getValue();
        }

        float getVoltageOffset() {
            return voltageOffset;
        }


        float getVoltageMultiplier() {
            return voltageMultiplier;
        }

        float getVoltage(float maxVoltage = 5.0) {
            return voltageOffset + (input->getValue() * (maxVoltage / 1023.0) * voltageMultiplier);
        }

        void setVoltageMultiplier(float multiplier) {
            this->voltageMultiplier = multiplier;
        }

        void setVoltageOffset(float offset) {
            this->voltageOffset = offset;
        }

        void printSensor() {
            Serial.print("AnalogInput on pin ");
            Serial.print(pin);
        }

        void debugPrint() {
            printSensor();
            Serial.print(": S=");
            Serial.print(this->getRawValue());
            Serial.print(": V=");
            Serial.print(this->getVoltage(), 3);
            Serial.print(" C=");
            Serial.println(this->getValue());
        }

};

#endif

#define ANALOG_INPUT_LOADED