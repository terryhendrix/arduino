#ifndef ANALOG_INPUT_LOADED
    #include <AnalogInput.h>
#endif

/**
    Light Dependent Resistor:
    http://www.hackerstore.nl/Artikel/57
*/
class LDR : public AnalogInput {
    public:
        LDR(int pin, int accuracy, unsigned long forceUpdateEveryMillis, unsigned long readIntervalMillis, bool pullup = false)
            : AnalogInput(pin, accuracy, forceUpdateEveryMillis, readIntervalMillis, pullup) { /** ... code ..*/}

        void printSensor() {
            #ifdef MY_DEBUG
                Serial.print("LDR on pin ");
                Serial.print(pin);
            #endif
        }

        void debugPrint() {
            #ifdef MY_DEBUG
                printSensor();
                Serial.print(": S=");
                Serial.print(this->getRawValue());
                Serial.print(": V=");
                Serial.print(this->getVoltage(), 3);
                Serial.print(" C=");
                Serial.println(this->getValue());
            #endif
        }

};