#ifndef SENSOR_BASE_LOADED
    #include <SensorBase.h>
#endif

class PotMeter : public Sensor {
  private:
    IntValue* potentio = new IntValue();
    int pin, accuracy;
    unsigned long lastReadMillis = 0,
                  readEveryMilis = 0,
                  minimumValue;
    double factor;

  public:
    PotMeter(int pin, int accuracy, unsigned long readEveryMilis, unsigned long minimumValue, unsigned long maximumValue) {
      this->pin = pin;
      this->accuracy = accuracy;
      this->readEveryMilis = readEveryMilis;
      this->minimumValue = minimumValue;
      this->factor =  ((double) maximumValue - (double) minimumValue) / ((double) getMaxPotentio() * (double) getMaxPotentio());
    }

    int getMaxPotentio() {
      return (1023 / accuracy * accuracy);
    }

    bool update(unsigned long epoch) {
      if(epoch - this->lastReadMillis > readEveryMilis) {
        this->lastReadMillis = epoch;
        potentio->setValue(analogRead(this->pin) / this->accuracy * this->accuracy);
      }

      return potentio->hasUpdated();
    }

    IntValue* getPotentio() {
      return potentio;
    }

    unsigned long getValue() {
      unsigned long x = potentio->getValue();
      return (unsigned long) x * x * this->factor + minimumValue;
    }

    #ifdef MY_DEBUG
    void debugPrint() {
      Serial.print("PotMeter on pin ");
      Serial.print(pin);
      Serial.print(": POT=");
      Serial.print(potentio->getValue());
      Serial.print(" V=");
      Serial.println(getValue());
    }
    #endif
};