#include <DHT.h>

#ifndef SENSOR_BASE_LOADED
    #include <SensorBase.h>
#endif

/**
 * PIN 1: VCC
 * PIN 2: DATA OUT
 * PIN 3: NOT USED
 * PIN 4: GROUND
 */
class Dht11 : public Sensor {
   private:
    static const bool CELCIUS = false;

    FloatValue* temperature = new FloatValue();
    FloatValue* humidity = new FloatValue();
    DHT* dht;
    unsigned long lastUpdateMillis = 0;
    unsigned long lastReadMillis = 0;
    unsigned long forceUpdateEveryMillis;
    unsigned long readIntervalMillis;
    float tempOffsetCelcius = 0;
    int pin;

  public:
    Dht11(int pin, unsigned long forceUpdateEveryMillis, unsigned long readIntervalMillis, float tempOffsetCelcius) {
      this->forceUpdateEveryMillis = forceUpdateEveryMillis;
      dht = new DHT(pin, DHT11);
      this->pin = pin;
      if(readIntervalMillis >= 2500) {
        this->readIntervalMillis = readIntervalMillis;
      }
      else {
        Serial.print("Warning, forcing read was ");
        Serial.println(readIntervalMillis);
        Serial.println("Corrected forcing read to 2500");
        this->readIntervalMillis = 2500;
      }

      this->tempOffsetCelcius = tempOffsetCelcius;

      #ifdef MY_DEBUG
        Serial.print("DHT11 on pin ");
        Serial.print(pin);
        Serial.print(", forcing updates every ");
        Serial.print(forceUpdateEveryMillis);
        Serial.println(" millis");
      #endif
    }

    /**
     * Get the measured temperature - the tempOffsetCelcius passed in the constructor
     */
    float getTemperature() {
      return temperature->getValue() - tempOffsetCelcius;
    }

    float getHumidity() {
      return humidity->getValue();
    }

    float getHeatIndex() {
      return dht->computeHeatIndex(getTemperature(), getHumidity(), CELCIUS);
    }

    bool update(unsigned long epoch) {
      dht->read(true);
      if(epoch - lastReadMillis > readIntervalMillis || getTemperature() == 0.0f || getHumidity() == 0.0f) {
        lastReadMillis = epoch;
        humidity->setValue(dht->readHumidity(true));
        temperature->setValue(dht->readTemperature(CELCIUS, true));

        #ifdef MY_DEBUG
          printSensor();
          Serial.println(": Reading");
        #endif
      }
      else {
        // Expire the update
        humidity->setValue(humidity->getValue());
        temperature->setValue(temperature->getValue());
      }

      if(epoch - lastUpdateMillis > forceUpdateEveryMillis || temperature->hasUpdated() || humidity->hasUpdated()) {
        lastUpdateMillis = epoch;
        return true;
      }
      else {
        return false;
      }
    }

    #ifdef MY_DEBUG
    void printSensor() {
      Serial.print("DHT11 on pin ");
      Serial.print(pin);
    }

    void debugPrint() {
      printSensor();
      Serial.print(": TM=");
      Serial.print(temperature->getValue());
      Serial.print(": TC=");
      Serial.print(getTemperature());
      Serial.print("°C, H=");
      Serial.print(getHumidity());
      Serial.print("%, HIC=");
      Serial.print(getHeatIndex());
      Serial.println("°C");
    }
    #endif
};