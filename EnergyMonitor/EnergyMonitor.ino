/**
Start domoticz to test:
  docker run -d -p 8080:8080 --name=domoticz -v /tmp/domoticz:/config -v /etc/localtime:/etc/localtime:ro sdesbure/domoticz
*/

#define MY_DEBUG
//#define WATER_DEBUG
//#define POWER_DEBUG
#define MY_RADIO_NRF24
#define MY_NODE_ID 3

#include <SPI.h>
#include <MySensors.h> 
#include <LDR.h>  
#include <PowerMeter.h>  
#include <WaterMeter.h>  
#include <MyEEPROM.h>

// MySensors children
#define CHILD_ID_KWH_HIGH           0
#define CHILD_ID_WATT               1
#define CHILD_ID_KWH_LOW            2
#define CHILD_ID_MODE               3
#define CHILD_ID_WATER_FLOW         4
#define CHILD_ID_WATER_VOLUME       5

#define FORCE_UPDATE_MILLIS         120000
#define SEND_MESSAGE_EVERY_WATT     5  
#define SEND_MESSAGE_EVERY_LITER    5  

// Power constants
#define PIN_LDR                     A0  // GRAY cable
#define LRD_READ_MILLIS             3
#define LDR_ACCURACY                20
#define POWER_LED_ON                60    // 60 when installed    
#define POWER_LED_OFF               40    // 40 when installed
#define TICKS_IN_KWH                1000
#define KWH_SAMPLE_INTERVAL         300000

// Water constants
#define PIN_REFLECTION              A1    // blue cable
#define REFLECTION_ACCURACY         5
#define REFLECTION_READ_COUNT       4
#define WATER_DISK_ON               140    // 60 when installed    
#define WATER_DISK_OFF              100    // 40 when installed
#define TICKS_IN_CUBIC_METER        2000
#define WATER_SAMPLE_INTERVAL       60000

// Gas constant
#define PIN_GAS_RESERVERD           A2    // green cable

#define EEPROM_ADDRESS_KWH_HIGH     0     // plus 3 cause its an unsiged long
#define EEPROM_ADDRESS_KWH_LOW      4     // plus 3 cause its an unsiged long
#define EEPROM_ADDRESS_WATER        8     // plus 3 cause its an unsiged long
#define EEPROM_ADDRESS_GAS_RESERVED 12    // plus 3 cause its an unsiged long

#define WRITE_TO_EEPROM_EVERY_LITER 500   // every 500 liter is stored
#define WRITE_TO_EEPROM_EVERY_WATT  2000  // every 2 kwh is stored

LDR* ldr; 
AnalogInput* reflection;
PowerMeter* power;
WaterMeter* water;

MyMessage msgKwhHigh(CHILD_ID_KWH_HIGH, V_KWH);
MyMessage msgWatt(CHILD_ID_WATT, V_WATT);
MyMessage msgKwhLow(CHILD_ID_KWH_LOW, V_KWH);
MyMessage msgMode(CHILD_ID_MODE, V_STATUS);

MyMessage msgWaterFlow(CHILD_ID_WATER_FLOW, V_FLOW);
MyMessage msgWaterVolume(CHILD_ID_WATER_VOLUME, V_VOLUME);

void presentation()  
{ 
  sendSketchInfo("EnergyMonitor", "1.0");
  present(CHILD_ID_KWH_HIGH, S_POWER);
  present(CHILD_ID_WATT, S_POWER);
  present(CHILD_ID_KWH_LOW, S_POWER);
  present(CHILD_ID_MODE, S_BINARY);
  present(CHILD_ID_WATER_FLOW, S_WATER);
  present(CHILD_ID_WATER_VOLUME, S_WATER);
}

void setup()
{
  Serial.println("Starting EnergyMonitor");
  #ifdef MY_DEBUG
    Serial.println("WARNING: Running with debug, incoming messages may not be received or to be delayed");
  #endif
  
  ldr = new LDR(PIN_LDR, LDR_ACCURACY, FORCE_UPDATE_MILLIS, LRD_READ_MILLIS);
  ldr->setMillisDelayAfterRead(0);
  power = new PowerMeter(TICKS_IN_KWH, KWH_SAMPLE_INTERVAL);
  power->setKwhHigh(EEPROM::getDouble(EEPROM_ADDRESS_KWH_HIGH));
  power->setKwhLow(EEPROM::getDouble(EEPROM_ADDRESS_KWH_LOW));
  
  reflection = new AnalogInput(PIN_REFLECTION, REFLECTION_ACCURACY, FORCE_UPDATE_MILLIS, REFLECTION_READ_COUNT, true);
  reflection->setMillisDelayAfterRead(3);
  
  water = new WaterMeter(TICKS_IN_CUBIC_METER, WATER_SAMPLE_INTERVAL);
  water->setCubicMeters(EEPROM::getDouble(EEPROM_ADDRESS_WATER));
  
  send(msgKwhHigh.set(power->getKwhHigh(), 3));
  send(msgKwhLow.set(power->getKwhLow(), 3));    
  send(msgWaterVolume.set(water->getCubicMeters(), 3));
}

void receive(const MyMessage &message) {
  if(message.sensor == CHILD_ID_KWH_HIGH) {
    power->setKwhHigh(message.getFloat());
    EEPROM::setDouble(EEPROM_ADDRESS_KWH_HIGH, power->getKwhHigh());
    send(msgKwhHigh.set(power->getKwhHigh(), 3));

    Serial.print("Setting HIGH Kw/h to ");
    Serial.println(power->getKwhHigh());
  }
  else if(message.sensor == CHILD_ID_KWH_LOW) {
    power->setKwhLow(message.getFloat());
    EEPROM::setDouble(EEPROM_ADDRESS_KWH_LOW, power->getKwhLow());
    send(msgKwhLow.set(power->getKwhLow(), 3));
    
    Serial.print("Setting LOW Kw/h to ");
    Serial.println(power->getKwhLow());
  }
  else if(message.sensor == CHILD_ID_WATER_VOLUME) {
    water->setCubicMeters(message.getFloat());
    EEPROM::setDouble(EEPROM_ADDRESS_WATER, water->getCubicMeters());
    send(msgWaterVolume.set(water->getCubicMeters(), 3));
    
    Serial.print("Setting Water cubic meters ");
    Serial.println(water->getCubicMeters());
  }
  else if(message.sensor == CHILD_ID_MODE && message.getInt() == HIGH) {
    power->setHigh();
    sendMode();
  }
  else if(message.sensor == CHILD_ID_MODE && message.getInt() == LOW) {
    power->setLow();
    sendMode();
  }
  else {
    Serial.print("Received message for sensor ");
    Serial.print(message.sensor);
    Serial.print(": ");
    Serial.println(message.getString());
  }
}

void loop()      
{  
  measurePower();
  measureWater();
}

void measureWater() {
  static bool ledOn = false;
  if(water->getSampler()->isSampleComplete()) {
    send(msgWaterFlow.set(water->getLiterSample(), 1));
  }

  if(reflection->update(millis())) {
    int v = reflection->getValue();
    if(v <= WATER_DISK_OFF && ledOn) {
      ledOn = false;
      
      Serial.print(millis() / 1000);
      Serial.print(": ");
      Serial.println("Water disk off");
    }
    else if(v >= WATER_DISK_ON && !ledOn) {
      ledOn = true;
      water->tick();
      
      Serial.print(millis() / 1000);
      Serial.print(": ");
      Serial.println("Water disk on");
      
      if(water->getTicks() % SEND_MESSAGE_EVERY_LITER == 0) {
        send(msgWaterVolume.set(water->getCubicMeters(), 3));
      }

      if(water->getTicks() != 0 && water->getTicks() % WRITE_TO_EEPROM_EVERY_LITER == 0) {
        Serial.println("Writing to water usage to EEPROM.");
        EEPROM::setDouble(EEPROM_ADDRESS_WATER, water->getCubicMeters());
      }
    }
    
    #ifdef WATER_DEBUG
      reflection->debugPrint();
    #endif
  }
}


void measurePower() {
  static bool ledOn = false;
  if(power->getSampler()->isSampleComplete()) {
    send(msgWatt.set(power->getWattSample(), 1));
    sendMode();
  }

  if(ldr->update(millis())) {
    int v = ldr->getValue();
    if(v <= POWER_LED_OFF && ledOn) {
      ledOn = false;
      Serial.print(millis() / 1000);
      Serial.print(": ");
      Serial.println("Power led off");
    }
    else if(v >= POWER_LED_ON && !ledOn) {
      ledOn = true;
      power->tick();
      
      Serial.print(millis() / 1000);
      Serial.print(": ");
      Serial.println("Power led on");
    
      if(power->getTicks() % SEND_MESSAGE_EVERY_WATT == 0) {
        send(msgKwhHigh.set(power->getKwhHigh(), 3));
        send(msgKwhLow.set(power->getKwhLow(), 3));
      }
      
      if(power->getTicks() != 0 && power->getTicks() % WRITE_TO_EEPROM_EVERY_WATT == 0) {
        Serial.println("Writing to high and low power usage to EEPROM.");
        EEPROM::setDouble(EEPROM_ADDRESS_KWH_HIGH, power->getKwhLow());
        EEPROM::setDouble(EEPROM_ADDRESS_KWH_LOW, power->getKwhHigh());
      }
    }
    
    #ifdef POWER_DEBUG
      ldr->debugPrint();
    #endif
  }
}

void sendMode() {
  if(power->isHighActive()) {
    send(msgMode.set(HIGH));
  }
  else{
    send(msgMode.set(LOW));
  }
}

