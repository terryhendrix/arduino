/** 
 *  FIX_MODE: The color doesn't change.
 *  RANDOM_HUE_MODE: Color fades to random hue.
 *  RAINBOW_HUE_MODE: Fading from red to red, going through all hue values.
 *  RED_MODE: Random fade between redish colors.
 *  BLUE_MODE: Random fade between blueish colors.
 *  GREEN_MODE: Random fade between greenish colors.
 *  FIRE_MODE: Fire effect. Like an electronic candle.
 */

const int redPin = 3;
const int greenPin = 5;
const int bluePin = 6;
const int ticks = 20;
#include <RGBMood.h>

RGBMood* m;

void setup() {
  Serial.begin(9600);
  Serial.println("Starting");
  
  m = new RGBMood(redPin, greenPin, bluePin, true);
  m->setMode(RGBMood::FIX_MODE);  // Automatic random fade.
  m->setHoldingTime(4000);  // Keep the same color for 4 seconds before fading again.
  m->setFadingSteps(ticks);   // Fade with 150 steps.
  m->setFadingSpeed(10);    // Each step last 50ms. A complete fade takes 50*150 = 7.5 seconds
}

void loop() {
  Serial.println("Red");
  m->setRGB(255,0,0);
  m->forceTick(1);
  m->debugPrint();
  delay(1000);
  
  Serial.println("Green");
  m->setRGB(0,255,0);
  m->forceTick(1);
  m->debugPrint();
  delay(1000);
  
  Serial.println("Blue");
  m->setRGB(0,0,255);
  m->forceTick(1);
  m->debugPrint();
  delay(1000);
  
  Serial.println("Off");
  m->setRGB(0,0,0);
  m->forceTick(1);
  m->debugPrint();
  delay(1000);

  Serial.println("Pulse REd");
  m->fadeRGB(255,0,0);
  m->forceTick(ticks);
  m->fadeRGB(0,0,0);
  m->forceTick(ticks);
  
  Serial.println("Pulse Green");
  m->fadeRGB(0,0,255);
  m->forceTick(ticks);
  m->fadeRGB(0,0,0);
  m->forceTick(ticks);
  
  Serial.println("Pulse Blue");
  m->fadeRGB(0,255,0);
  m->forceTick(ticks);
  m->fadeRGB(0,0,0);
  m->forceTick(ticks);
  
  Serial.println("Green");
  m->setRGB(0,255,0);
  m->forceTick(1);
  m->debugPrint();
  delay(1000);
}


