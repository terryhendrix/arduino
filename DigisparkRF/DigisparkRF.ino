
// https://digistump.com/wiki/digispark/tutorials/basics
// All pins can be used as Digital I/O
// Pin 0 → I2C SDA, PWM (LED on Model B)
// Pin 1 → PWM (LED on Model A)
// Pin 2 → I2C SCK, Analog In
// Pin 3 → Analog In (also used for USB+ when USB is in use)
// Pin 4 → PWM, Analog (also used for USB- when USB is in use)
// Pin 5 → Analog In
// https://digistump.com/wiki/digispark/tutorials/basics
// Pin usage:
//  AnalogIn: p2 = 1, p5 = 0         
//  USB: p3 = 3, p4 = 2
//  AnalogOut: p0 = 0, p1 = 1, p4 = 4
//};

/*
Manchester Transmitter example
   In this example transmitter will send one 16 bit number per transmission  

try different speeds using this constants, your maximum possible speed will depend on various factors like transmitter type, distance, microcontroller speed, ...

  MAN_300 0
  MAN_600 1
  MAN_1200 2
  MAN_2400 3
  MAN_4800 4
  MAN_9600 5
  MAN_19200 6
  MAN_38400 7

*/

#include <Manchester.h>
#include <DigiUSB.h>

#define TX_PIN 0  //pin where your transmitter is connected

uint16_t transmit_data = 2761;


void setup() {
  pinMode(1, OUTPUT);   // RF Transmitter
  man.setupTransmit(TX_PIN, MAN_1200);
  DigiUSB.begin();    
}

void loop() {
  DigiUSB.println("RxRF");
  man.transmit(transmit_data);
  
//  delay(500);
//  digitalWrite(1, LOW); // LED OF
//  delay(500);
//  digitalWrite(1, HIGH); // LED ON
  DigiUSB.delay(20);
  DigiUSB.refresh();
}
