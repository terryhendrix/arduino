/**
Start domoticz to test:
  docker run -d -p 8080:8080 --name=domoticz -v /tmp/domoticz:/config -v /etc/localtime:/etc/localtime:ro sdesbure/domoticz
 */

#define MY_DEBUG
#define MY_RADIO_NRF24
//#define MY_NODE_ID 4       used to set a static node id into a device, must be unique

#include <SPI.h>
#include <MySensors.h> 
#include <IRremote.h>
#include <MotionSensor.h>  

#define PIN_PIR         4
#define PIN_IR_TX       3
#define PIN_IR_RX       8

#define CHILD_ID_IR_RX  0
#define CHILD_ID_PIR    1
#define CHILD_ID_IR_TX  2

#define IR_KHZ          38

MyMessage msgIrRx(CHILD_ID_IR_RX, V_TEXT);
MyMessage msgIrTx(CHILD_ID_IR_TX, V_TEXT);
MyMessage msgPir(CHILD_ID_PIR, V_TRIPPED);

IRrecv irrecv(PIN_IR_RX);
IRsend irsend;
decode_results results;

MotionSensor* motion;
String hex;

void presentation()  
{ 
  sendSketchInfo("SensorHub", "1.0");
  present(CHILD_ID_IR_TX, S_INFO);
  present(CHILD_ID_IR_RX, S_INFO);
  present(CHILD_ID_PIR, S_MOTION);
}

void setup()
{
  #ifdef MY_DEBUG
    Serial.println("Starting SensorHub");
  #endif
  
  motion = new MotionSensor(PIN_PIR);

  irsend.enableIROut(IR_KHZ); 
  
  wait(1000);
  irsend.sendNEC(2090123,32);
  wait(1000);
  irsend.sendNEC(2090378,32);
  wait(1000);
  
  irrecv.enableIRIn(); // Start the receiver
  irrecv.blink13(true);
}

void receive(const MyMessage &message) {
  if(message.sensor == CHILD_ID_IR_TX) {
    String cmd = String(message.getString());
    int commaIndex = cmd.indexOf(',');
    int secondCommaIndex = cmd.indexOf(',', commaIndex+1);
    
    String hexStr = cmd.substring(0, commaIndex);
    String decodeTypeStr = cmd.substring(commaIndex+1, secondCommaIndex);
    String bitsStr = cmd.substring(secondCommaIndex+1);


    char* hex = new char[hexStr.length()];
    hexStr.toCharArray(hex, hexStr.length());
    unsigned long value = strtoul(hex, 0, 16);
    
    #ifdef MY_DEBUG 
    {
      Serial.print(decodeTypeStr);
      Serial.print(" IR TX ");
      Serial.print(value);
      Serial.print(" (");
      Serial.print(bitsStr);
      Serial.print(" bits) HEX: ");
      Serial.println(hexStr);
    } 
    #endif
    
    irsend.enableIROut(IR_KHZ); 
    
    irsend.sendNEC(value,32);
    
    irrecv.enableIRIn(); 
  }
  else {
    #ifdef MY_DEBUG
      Serial.print("Received message for sensor ");
      Serial.print(message.sensor);
      Serial.print(": ");
      Serial.println(message.getString());
    #endif
  }
}

void loop()      
{
  receiveIr();
  detectMotion();  
}

void detectMotion() {
  if(motion->update(millis())) {
    #ifdef MY_DEBUG
      motion->debugPrint();
    #endif
    
    send(msgPir.set(motion->isMovementDetected()));
  }
}

bool receiveIr() {
  
  if(irrecv.decode(&results))
  { 
    irrecv.resume(); // Receive the next value
    hex = String(results.value, HEX);
    hex.toUpperCase();
    String decode_type = decodeType(results.decode_type);
    send(msgIrRx.set(hex + "," + decode_type + "," + results.bits));

    if(validHex(hex)) {
      #ifdef MY_DEBUG
        Serial.print(decode_type);
        Serial.print(" IR RX ");
        Serial.print(results.value);
        Serial.print(" (");
        Serial.print(results.bits);
        Serial.print(" bits) HEX: ");
        Serial.println(hex);
      #endif
      return true;
    }
    else {
      return false;
    }
  } 
  else {
    return false;
  }
}

bool validHex(String hex) {
  bool valid = false;
  unsigned int s = hex.length();
  for(unsigned int i = 0; i < s; i++) {
    if(hex.charAt(i) != 'F' && hex.charAt(i) != 'f') {
      valid = true;
    }
  }
  return valid;
}

String decodeType(int decode_type) {
  if(decode_type == NEC) {
    return "NEC";;
  }
  else if(decode_type == SONY) {
    return "SONY";
  }
  else if(decode_type == RC5) {
    return "RC5";
  }
  else if(decode_type == RC6) {
    return "RC6";
  }
  else if(decode_type == UNKNOWN) {
    return "UNKNOWN";
  }
}



