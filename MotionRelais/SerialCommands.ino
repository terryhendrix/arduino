
void handleSerialCommands() {
  // if time sync available from serial port, update time and return true
  while(Serial.available()){  // time message consists of header & 10 ASCII digits
    char c = Serial.read(); 
    if(c == 'T') {
      unsigned long epoch = toLong(readLineFromSerial());
      unsigned long timezoneOffset = 2L * 60L * 60L;
      Serial.print("Time set to ");
      printDate();
      Serial.println();
    } 
    else {
      Serial.print("Warning: Unknown command: ");
      Serial.print(c);
      Serial.println(readLineFromSerial());
      Serial.println("Available       Example");
      Serial.println("T<EPOCH>        T1472997997");
    }
  }
}

String readLineFromSerial() {
  String command = "";
  char c;
  while(Serial.available()){
    c = Serial.read(); 
    command = String(command + c);
  }
  return command;
}


long toLong(String s) {
  unsigned long _long = 0;
  for (unsigned int i = 0; i < s.length(); i++) {
     char c = s.charAt(i);
     
     if (c < '0' || c > '9') 
      break;
      
     _long *= 10;
     _long += (c - '0');
  }
  return _long;
}

