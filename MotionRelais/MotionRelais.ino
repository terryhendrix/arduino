// Enable an output based on a PIR sensor, adding EXTRA_DELAY before turning the output back off.
// Set the time from serial: T1472997997

#define MY_DEBUG

// Enable and select radio type attached 
#define MY_RADIO_NRF24
//#define MY_RADIO_RFM69
//#define MY_RS485

#include <SPI.h>
#include <MySensors.h>  
#include <DHT.h>

#include      <Time.h>
#define       DEBUG                       true
#define       PIN_SENSOR                  2         // Pen waar bewegingssensor aan zit
#define       PIN_OUTPUT                  9         // Pen waar de een V5 digitale output zit
#define       PIN_POTMETER                A2        // Potmeter for delay
#define       MIN_EXTRA_DELAY             30000     // The extra delay (on top of PIR delay)
#define       MAX_EXTRA_DELAY             2000000   // The extra delay (on top of PIR delay)
#define       EXCLUDED_START              0         // Hour value 0 will no longer trigger
#define       EXCLUDED_STOP               8         // Hour value + 1 will trigger again the relais again
#define       SLEEP_WHEN_OFF              150
#define       SLEEP_WHEN_ON               1000
#define       SLEEP_WHEN_EXCLUDED         15000
#define       BAUD_RATE                   9600

void setup() {
  pinMode(PIN_SENSOR, INPUT);
  pinMode(PIN_OUTPUT, OUTPUT); 
  
  Serial.begin(BAUD_RATE);
  
  #if DEBUG == true
    printDate();
    Serial.println(": Starting");
  #endif
}

void loop() { 
  handleSerialCommands();
  if(exclusionWindowActive()) {
    logExclusion();
    digitalWrite(PIN_OUTPUT,LOW); 
    sleep(SLEEP_WHEN_EXCLUDED);
  }
  else if(movementDetected(PIN_SENSOR)) {
    digitalWrite(PIN_OUTPUT,HIGH);
    sleep(SLEEP_WHEN_ON);
  }
  else {
    digitalWrite(PIN_OUTPUT,LOW); 
    sleep(SLEEP_WHEN_OFF);      // TODO: When MySensors network is running, date may be published, so we can go in deep sleep here
  }
}

bool exclusionWindowActive() {
  return EXCLUDED_START <= hour() && EXCLUDED_STOP >= hour();
}

/**
 * @param   lastMovementMillis  The lastMovement that was detected
 */
bool movementDetected(int pin)  {
  static byte moving = 0;
  static byte sensor = 0;
  static unsigned long lastMovementMillis = 0;
  
  sensor = digitalRead(pin);
  
  if(sensor == LOW && moving == HIGH && movementTimedOut(lastMovementMillis)) {
    moving = LOW;
    debug(HIGH, LOW, lastMovementMillis);
  }
  else if(sensor == LOW && moving == HIGH) {
    printDate();
    Serial.println(": Delaying off");
  }
  else if(sensor == HIGH && moving == LOW) {
    moving = HIGH;
    lastMovementMillis = millis();
    debug(LOW, HIGH, lastMovementMillis);
  }

  return moving == HIGH;
}
