bool movementTimedOut(unsigned long lastMovementMillis) {
  unsigned long delay = sleepDelay(potmeterValue());
  unsigned long m = millis();
  return m - lastMovementMillis > delay;
}

/**
 * Potmeter value may be between 0 and 1023
 */
unsigned long sleepDelay(unsigned long potmeterValue) {
  return MIN_EXTRA_DELAY + potmeterValue * (MAX_EXTRA_DELAY / 1024);
}

/**
 * Returns a value between 0 and 1023
 */
unsigned long potmeterValue() {
  unsigned long potmeter = 0;
  for(int i = 0; i < 8; i++) {
    potmeter += analogRead(PIN_POTMETER);
    delay(60);
  }
  return potmeter / 8;
}

