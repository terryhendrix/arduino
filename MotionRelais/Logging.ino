
void debug(int prev, int curr, unsigned long lastMovementMillis) {
  if(timeStatus() == timeNotSet) {
    Serial.println("Warning: Time has not been set");
  }
  else if(timeStatus() == timeNeedsSync) {
    Serial.println("Warning: Time needs sync");
  }

  printDate();
  Serial.print(": "); 
  Serial.print(prev); 
  Serial.print(" => "); 
  Serial.print(curr);
  Serial.print(", movement = "); 
  Serial.print(lastMovementMillis); 
  Serial.print(", delay = "); 
  Serial.print(sleepDelay(analogRead(PIN_POTMETER))); 
  Serial.println();
}

void printDate() {
  printDigits(hour());
  Serial.print(":");
  printDigits(minute());
  Serial.print(":");
  printDigits(second());
  Serial.print(" ");
  printDigits(day());
  Serial.print("-");
  printDigits(month());
  Serial.print("-");
  Serial.print(year()); 
}

void printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void logExclusion() {
  printDate();
  Serial.print(": Exlusion window (");
  printDigits(EXCLUDED_START);
  Serial.print("-");
  printDigits(EXCLUDED_START);
  Serial.print(") active, current hour is ");
  printDigits(hour());
  Serial.println();
}

