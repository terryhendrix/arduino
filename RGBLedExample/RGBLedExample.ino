

const int redPin = 3;
const int greenPin = 5;
const int bluePin = 6;
#include <RGBMood.h>

RGBMood* m;

void setup() {
  Serial.begin(9600);
  Serial.println("Starting");
  
  m = new RGBMood(redPin, greenPin, bluePin, true);
}

void loop() {
  modeExample();
}

/** 
 *  FIX_MODE: The color doesn't change.
 *  RANDOM_HUE_MODE: Color fades to random hue.
 *  RAINBOW_HUE_MODE: Fading from red to red, going through all hue values.
 *  RED_MODE: Random fade between redish colors.
 *  BLUE_MODE: Random fade between blueish colors.
 *  GREEN_MODE: Random fade between greenish colors.
 *  FIRE_MODE: Fire effect. Like an electronic candle.
 */
void modeExample() {
  m->setMode(RGBMood::RED_MODE);  // Automatic random fade.
  m->setHoldingTime(5);  // Keep the same color for 4 seconds before fading again.
  m->setFadingSteps(150);   // Fade with 150 steps.
  m->setFadingSpeed(10);    // Each step last 50ms. A complete fade takes 50*150 = 7.5 seconds
  m->forceTick(5000);
}

void fixModeExample() {
  int ticks = 10;
  int speed = 30;
  
  m->setMode(RGBMood::FIX_MODE);  // Automatic random fade.
  m->setHoldingTime(5);  // Keep the same color for 4 seconds before fading again.
  m->setFadingSteps(ticks);   // Fade with 150 steps.
  m->setFadingSpeed(speed);    // Each step last 50ms. A complete fade takes 50*150 = 7.5 seconds
  
  Serial.println("Red");
  m->setRGB(255,0,0);
  m->forceTick(ticks);
  m->debugPrint();
  
  Serial.println("Green");
  m->setRGB(0,255,0);
  m->forceTick(ticks);
  m->debugPrint();
  
  Serial.println("Blue");
  m->setRGB(0,0,255);
  m->forceTick(ticks);
  m->debugPrint();
  
  Serial.println("Off");
  m->setRGB(0,0,0);
  m->forceTick(ticks);
  m->debugPrint();

  Serial.println("Pulse Red");
  m->fadeRGB(255,0,0);
  m->forceTick(ticks * 2);
  m->fadeRGB(0,0,0);
  m->forceTick(ticks * 2);
  
  Serial.println("Pulse Green");
  m->fadeRGB(0,0,255);
  m->forceTick(ticks * 2);
  m->fadeRGB(0,0,0);
  m->forceTick(ticks * 2);
  
  Serial.println("Pulse Blue");
  m->fadeRGB(0,255,0);
  m->forceTick(ticks * 2);
  m->fadeRGB(0,0,0);
  m->forceTick(ticks * 2);

  Serial.println("HSB 0, 255, 255");
  delay(1000);
  
  for(int bri = 0; bri < 255; bri += 60) {
    for(int hue = 0; hue < 360; hue += 60) {
      m->fadeHSB(hue, bri, bri, true);
      m->forceTick(ticks * 2);
    }
  }
  
  for(int bri = 255; bri >= 0; bri -= 60) {
    for(int hue = 0; hue < 360; hue += 60) {
      m->fadeHSB(hue, bri, bri, true);
      m->forceTick(ticks * 2);
    }
  }
  
  m->fadeRGB(0,0,0);
  m->forceTick(ticks * 2);
}

