/**
Start domoticz to test:
  docker run -d -p 8080:8080 --name=domoticz -v /tmp/domoticz:/config -v /etc/localtime:/etc/localtime:ro sdesbure/domoticz
 */

#define MY_DEBUG
#define MY_RADIO_NRF24

#include <SPI.h>
#include <MySensors.h> 
#include <MotionSensor.h>  
#include <Relay.h>  

#define CHILD_ID_PIR            0
#define PIN_PIR                 3
#define CHILD_ID_RELAIS         1
#define PIN_RELAIS              7
#define FORCE_SEND_INTERVAL     15000  

Relais* relais;
MotionSensor* motion;

MyMessage msgPir(CHILD_ID_PIR, V_TRIPPED);
MyMessage msgRelais(CHILD_ID_RELAIS, V_STATUS);

void presentation()  
{ 
  sendSketchInfo("MotionRelay", "1.0");
  present(CHILD_ID_PIR, S_MOTION);
  present(CHILD_ID_RELAIS, S_LIGHT);
}

void setup()
{
  #ifdef MY_DEBUG
    Serial.println("Starting MotionRelay");
    Serial.println("WARNING: Running with debug, incoming messages may not be received");
  #endif
  
  relais = new Relais(PIN_RELAIS, false);
  motion = new MotionSensor(PIN_PIR, FORCE_SEND_INTERVAL);
  wait(1000);  
  relais->turnOff();
}

void receive(const MyMessage &message) {
  #ifdef MY_DEBUG
    Serial.print("Received message for sensor ");
    Serial.print(message.sensor);
    Serial.print(": ");
    Serial.println(message.getString());
  #endif

  if(message.sensor == CHILD_ID_RELAIS) {
    if(message.getInt() == HIGH) {
      relais->turnOn();
      send(msgRelais.set(HIGH));
      
      #ifdef MY_DEBUG
        relais->debugPrint();
      #endif
    }
    else if(message.getInt() == LOW) {
      relais->turnOff();
      send(msgRelais.set(LOW));
      
      #ifdef MY_DEBUG
        relais->debugPrint();
      #endif
    }
  }
}

void loop()      
{  
  if(motion->update(millis())) {
    send(msgPir.set(motion->isMovementDetected()));
  }

  if(motion->isMovementDetected()) {
    wait(1500); // send update every 1.5 seconds
    send(msgPir.set(motion->isMovementDetected()));
  }

  #ifdef MY_DEBUG
    motion->debugPrint();
    wait(300);
  #endif
}

