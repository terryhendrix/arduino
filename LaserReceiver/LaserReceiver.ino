#include <avr/sleep.h>

#define PIN_LASER_RX  2   
#define PIN_LASER_TX  4   
#define PIN_ERROR_LED 8
#define PIN_OK_LED 7
#define RETEST_INTERVAL 5000

bool detected = false;
unsigned long lastTest = 0;
bool validTest = false;
 
void setup()
{
  Serial.begin(9600);
  Serial.println("LaserReceiver");
  
  pinMode(PIN_LASER_RX, INPUT);
  pinMode(PIN_LASER_TX, OUTPUT);
  pinMode(PIN_ERROR_LED, OUTPUT);
  pinMode(PIN_OK_LED, OUTPUT);
  
  runTest();
}


void loop()
{
  if(validTest) {
    sleepNow(); 
  }
  else if(!validTest && millis() - lastTest > RETEST_INTERVAL) {
    runTest();
  }
  else {  // delay until the next test is started
    delay(1000);    
  }
}

void readSensor()        
{
  if(digitalRead(PIN_LASER_RX) == HIGH) {
    Serial.println("Laser received");
    detected = false;
  }
  else {
    Serial.println("Laser interrupted, movement detected");
    detected = true;
  }
}

void runTest() {
  if(!selfTest()) {
    Serial.print("Error: Self test failed, retry in ");
    Serial.print(RETEST_INTERVAL);
    Serial.println(" millis");
    setStatusError();
  }
  else {
    Serial.println("Self test succesful");
    setStatusOK();
    sleepNow();
  }
}

bool selfTest() {
  bool success = true;
  for(int i = 0; i < 3; i++) {

    setStatusOK();
    disableLaser();
    readSensor();

    if(detected == false) {
      success = false;
      Serial.println("Warning: Should have detected movement");
    }
    
    setStatusError();
    enableLaser();
    readSensor();
    
    if(detected == true) {
      success = false;
      Serial.println("Warning: Should not have detected movement");
    }
  }

  lastTest = millis();
  validTest = success;
  return success;
}

void sleepNow()         // here we put the arduino to sleep
{
  Serial.println("Sleeping");
  Serial.flush();
  delay(300);
  set_sleep_mode(SLEEP_MODE_STANDBY);  
  sleep_enable();          
  attachInterrupt(0, readSensor, CHANGE); 
  sleep_mode();   // Go to sleep      
  sleep_disable();
  detachInterrupt(0);
  delay(300);
}

void enableLaser() {
  delay(200);
  digitalWrite(PIN_LASER_TX, HIGH);
}

void disableLaser() {
  delay(200);
  digitalWrite(PIN_LASER_TX, LOW);
}

void setStatusOK() {
    digitalWrite(PIN_ERROR_LED, LOW);
    digitalWrite(PIN_OK_LED, HIGH);
}

void setStatusError() {
    digitalWrite(PIN_OK_LED, LOW);
    digitalWrite(PIN_ERROR_LED, HIGH);
}

 
