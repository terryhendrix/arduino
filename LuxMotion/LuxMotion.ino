/**
Start domoticz to test:
  docker run -d -p 8080:8080 --name=domoticz -v /tmp/domoticz:/config -v /etc/localtime:/etc/localtime:ro sdesbure/domoticz
 */

#define MY_DEBUG
#define MY_RADIO_NRF24
#define MY_NODE_ID 5      // used to set a static node id into a device, must be unique

#include <SPI.h>
#include <MySensors.h> 
#include <MotionSensor.h>  
#include <LDR.h>  

#define PIN_PIR         3
#define PIN_LDR         A7

#define CHILD_ID_PIR            2
#define CHILD_ID_LIGHT_LEVEL    1

#define SLEEP_DURATION  120000

#define IR_KHZ          38

MyMessage msgPir(CHILD_ID_PIR, V_TRIPPED);
MyMessage msgLightLevel(CHILD_ID_LIGHT_LEVEL, V_LIGHT_LEVEL);

MotionSensor* motion;
LDR* ldr; 

void presentation()  
{ 
  sendSketchInfo("LuxMotion", "1.0");
  present(CHILD_ID_LIGHT_LEVEL, S_LIGHT_LEVEL);
  present(CHILD_ID_PIR, S_MOTION);
}

void setup()
{
  #ifdef MY_DEBUG
    Serial.println("Starting SensorHub");
  #endif
  
  motion = new MotionSensor(PIN_PIR);
  ldr = new LDR(PIN_LDR, 3, 0, 2);
}

void receive(const MyMessage &message) {
  #ifdef MY_DEBUG
    Serial.print("Received message for sensor ");
    Serial.print(message.sensor);
    Serial.print(": ");
    Serial.println(message.getString());
  #endif
}

void loop()      
{
  int result = sleep(digitalPinToInterrupt(PIN_PIR), CHANGE, random(SLEEP_DURATION, SLEEP_DURATION + 1000));
  if(result > 0) {
    #ifdef MY_DEBUG
      Serial.println("Woke up by signal change.");
    #endif
    
    detectMotion();
  }
  else {
    #ifdef MY_DEBUG
      Serial.println("Woke up by timer.");
    #endif
    
    send(msgPir.set(motion->isMovementDetected()));
  }
  
  readLux();
}

void readLux() {
  if(ldr->update(millis())) {
    send(msgLightLevel.set(ldr->getValue()));
  }
  #ifdef MY_DEBUG
    ldr->debugPrint();
  #endif
}

void detectMotion() {
  if(motion->update(millis())) {
    send(msgPir.set(motion->isMovementDetected()));
  }
  #ifdef MY_DEBUG
    motion->debugPrint();
  #endif
    
}



